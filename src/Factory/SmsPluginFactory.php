<?php

namespace paml\Notification\Sms\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Sms\Entity\Sms;
use paml\Notification\Sms\Plugin\SmsPlugin;
use Zend\ServiceManager\Factory\FactoryInterface;

class SmsPluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SmsPlugin(
            $container->get(EntityManager::class)->getRepository(Sms::class)
        );
    }
}

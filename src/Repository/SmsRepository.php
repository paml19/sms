<?php

namespace paml\Notification\Sms\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Notification\Sms\Entity\Sms;

class SmsRepository extends EntityRepository
{
    public function save(Sms $sms): void
    {
        $this->getEntityManager()->persist($sms);
        $this->getEntityManager()->flush();
    }

    public function getUnsent(int $itemsCount): ?array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('s')
            ->from($this->getEntityName(), 's')
            ->where($queryBuilder->expr()->isNull('s.dateSend'))
            ->setMaxResults($itemsCount)
            ->orderBy('s.dateAdd', 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }
}

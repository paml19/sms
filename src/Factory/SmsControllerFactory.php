<?php

namespace paml\Notification\Sms\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Sms\Controller\SmsController;
use paml\Notification\Sms\Entity\Sms;
use paml\Notification\Sms\Service\SmsService;
use Zend\ServiceManager\Factory\FactoryInterface;

class SmsControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SmsController(
            $container->get('Config')['notification']['items_count'],
            $container->get(SmsService::class),
            $container->get(EntityManager::class)->getRepository(Sms::class)
        );
    }
}

<?php

namespace paml\Notification\Sms\Service;

use paml\Notification\Sms\Entity\Sms;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;
use Smsapi\Client\SmsapiHttpClient;

class SmsService
{
    private $token;

    private $developmentMode;

    private $developmentRecipient;

    private $appName;

    public function __construct(
        string $token,
        bool $developmentMode,
        ?string $developmentRecipient,
        string $appName
    ) {
        $this->token = $token;
        $this->developmentMode = $developmentMode;
        $this->developmentRecipient = $developmentRecipient;
        $this->appName = $appName;
    }

    public function send(Sms $sms)
    {
        if ($this->developmentMode) {
            return $this->sendDevelopment($sms);
        }

        try {
            $message = SendSmsBag::withMessage(
                $sms->getRecipient(),
                $sms->getMessage()
            );
            $message->encoding = 'utf-8';

            $service = (new SmsapiHttpClient())
                ->smsapiPlService($this->token);
            $service->smsFeature()->sendSms($message);
        } catch (\RuntimeException $exception) {
            return $exception->getMessage();
        }
    }

    private function sendDevelopment(Sms $sms)
    {
        try {
            $message = SendSmsBag::withMessage(
                $this->developmentRecipient,
                '[' . $this->appName . '][' . $sms->getRecipient() . ']' . $sms->getMessage()
            );
            $message->encoding = 'utf-8';

            $service = (new SmsapiHttpClient())
                ->smsapiPlService($this->token);
            $service->smsFeature()->sendSms($message);
        } catch (\RuntimeException $exception) {
            return $exception->getMessage();
        }
    }
}

<?php

namespace paml\Notification\Sms\Controller;

use paml\Notification\Sms\Entity\Sms;
use paml\Notification\Sms\Repository\SmsRepository;
use paml\Notification\Sms\Service\SmsService;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class SmsController extends AbstractConsoleController
{
    private $itemsCount;

    private $smsService;

    private $smsRepository;

    public function __construct(
        int $itemsCount,
        SmsService $smsService,
        SmsRepository $smsRepository
    ) {
        $this->itemsCount = $itemsCount;
        $this->smsService = $smsService;
        $this->smsRepository = $smsRepository;
    }

    public function testAction()
    {
        $sms = (new Sms)
            ->setRecipient('534114552')
            ->setMessage('TEST');

        $this->smsRepository->save($sms);

        $this->smsService->send($sms);
        $sms->setDateSend(new \DateTime);

        $this->smsRepository->save($sms);
    }

    public function indexAction()
    {
        $messages = $this->smsRepository->getUnsent($this->itemsCount);

        /** @var Sms $message */
        foreach ($messages as $message) {
            $this->smsService->send($message);
            $message->setDateSend(new \DateTime);

            $this->smsRepository->save($message);
        }
    }
}

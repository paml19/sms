<?php

namespace paml\Notification\Sms\Plugin;

use paml\Notification\Sms\Entity\Sms;
use paml\Notification\Sms\Repository\SmsRepository;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class SmsPlugin extends AbstractPlugin
{
    private $smsRepository;

    /** @var Sms */
    private $sms;

    public function __construct(SmsRepository $smsRepository)
    {
        $this->smsRepository = $smsRepository;
    }

    public function __invoke(?Sms $sms = null): self
    {
        if ($sms) {
            $this->smsRepository->save($sms);
        } else {
            $this->sms = new Sms;
        }

        return $this;
    }

    public function setRecipient(string $recipient): self
    {
        $this->sms->setRecipient($recipient);
    }

    public function setMessage(string $message): self
    {
        $this->sms->setMessage($message);
    }

    public function save(): void
    {
        $this->smsRepository->save($this->sms);
    }
}

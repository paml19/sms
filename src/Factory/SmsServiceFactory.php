<?php

namespace paml\Notification\Sms\Factory;

use Interop\Container\ContainerInterface;
use paml\Notification\Sms\Service\SmsService;
use Zend\ServiceManager\Factory\FactoryInterface;

class SmsServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['notification']['sms'])) {
            throw new \Exception('No sms config provided!');
        }

        return new SmsService(
            $config['notification']['sms']['token'],
            $config['notification']['sms']['development_mode'],
            $config['notification']['sms']['development_recipient'],
            $config['notification']['app_name']
        );
    }
}

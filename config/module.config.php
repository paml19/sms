<?php

namespace paml\Notification\Sms;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'controllers' => [
        'factories' => [
            Controller\SmsController::class => Factory\SmsControllerFactory::class,
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            Plugin\SmsPlugin::class => Factory\SmsPluginFactory::class,
        ],
        'aliases' => [
            'sms-plugin' => Plugin\SmsPlugin::class,
            'sms' => Plugin\SmsPlugin::class,
            'smsPlugin' => Plugin\SmsPlugin::class,
            'smser' => Plugin\SmsPlugin::class,
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'notification-sms' => [
                    'options' => [
                        'route' => 'notification-sms',
                        'defaults' => [
                            'controller' => Controller\SmsController::class,
                            'action' => 'index',
                        ],
                    ],
                ],
                'notification-sms-test' => [
                    'options' => [
                        'route' => 'notification-sms-test',
                        'defaults' => [
                            'controller' => Controller\SmsController::class,
                            'action' => 'test',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\SmsService::class => Factory\SmsServiceFactory::class
        ],
    ],
];
